pub mod module;
pub mod error;
pub mod types;
pub mod instruction;

#[macro_export]
/// Create an InstructionSet with vec!-like syntax
macro_rules! instruction_set {
    ( $( $x:expr ),* ) => {
        {
            let mut temp = Vec::new();
            $( temp.push(Instruction::new($x.to_string())); )*
            temp
        }
    };
}

#[macro_export]
macro_rules! add_data {
    { $m:expr, $( $n:expr => $( $x:expr ),* ; ),* } => {
        $(
            let mut temp = Vec::new();
            $( temp.push($x); )*
            $m.add_data($n, temp);
        )*
    };
}

#[macro_export]
macro_rules! add_function {
    ( $m:expr, fn $n:expr => $t:expr => { $( $x:expr),* } ) => {
        let mut temp = Vec::new();
        $( temp.push(Instruction::new($x.to_string())); )*
        $m.add_function($n, $t, None, false, temp);
    };
    ( $m:expr, export fn $n:expr => $t:expr => { $( $x:expr ),* } ) => {
        let mut temp = Vec::new();
        $( temp.push(Instruction::new($x.to_string())); )*
        $m.add_function($n, $t, None, true, temp);
    };
    ( $m:expr, fn $n:expr => ( $( $a:expr ),* ) => $t:expr => { $( $x:expr ),* } ) => {
        let mut temp = Vec::new();
        let mut a = Vec::new();
        $( temp.push(Instruction::new($x.to_string())); )*
        $( a.push($a); )*
        $m.add_function($n, $t, Some(a), false, temp);
    };
    ( $m:expr, export fn $n:expr => ( $( $a:expr ),* ) => $t:expr => { $( $x:expr ),* } ) => {
        let mut temp = Vec::new();
        let mut a = Vec::new();
        $( temp.push(Instruction::new($x.to_string())); )*
        $( a.push($a); )*
        $m.add_function($n, $t, Some(a), true, temp);
    };
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::Write;

    use crate::module::Module;
    use crate::types::QbeType;
    use crate::instruction::Instruction;

    #[test]
    fn module_new() {
        let _ = Module::new();
    }

    #[test]
    fn instruction_new() {
        let i = Instruction::new(String::from("# nothing"));
        assert!(i.dump() != "");
    }

    #[test]
    fn add_function() {
        let mut module = Module::new();
        module.add_function("main", QbeType::W, None, false, instruction_set!("ret 0"));
    }

    #[test]
    fn dump() {
        let mut module = Module::new();
        module.add_raw(instruction_set!("# Comment"));
        assert_eq!(module.dump(), String::from("# Comment\n\n"));
    }

    #[test]
    fn add_data() {
        let mut module = Module::new();
        module.add_data("str", vec!["b \"hello world\"", "b 0"]);
    }

    #[test]
    fn hello() {
        let mut module = Module::new();
        module.add_data("str", vec!["b \"hello world\"", "b 0"]);
        let inst = instruction_set!("%r =w call $puts(l $str)", "ret 0");
        module.add_function("main", QbeType::W, None, true, inst);
    }

    #[test]
    fn add_data_macro() {
        let mut module = Module::new();
        add_data! { 
            module, 
            "str" => "b \"hello world\"", "b 0" ;
        };
        module.add_function("main", QbeType::W, None, true, instruction_set!("%r =w call $puts(l $str)", "ret 0"));
    }

    #[test]
    fn add_function_macro() {
        let mut module = Module::new();
        add_data! {
            module,
            "fmt" => "b \"One and one make %d!\\n\"", "b 0" ;,
            "str" => "b \"Hello world\"", "b 0" ;
        };

        add_function!(
            module,
            export fn "main" => QbeType::W => {
                "%r =w call $add(w 1, w 1)",
                "call $printf(l $fmt, w %r, ...)",
                "call $puts(l $str)",
                "ret 0"
            }
        );

        add_function!(
            module,
            fn "add" => ( "w %a", "w %b" ) => QbeType::W => {
                "%c =w add %a, %b",
                "ret %c"
            }
        );

        let mut f = File::create("hello.ssa").unwrap();
        f.write_all(module.dump().as_bytes()).ok();
    }
}
