#[derive(Debug)]
pub enum QbeType {
    W,
    L,
    S,
    D,
    B,
    H,
}

pub fn type_to_char(t: QbeType) -> char {
    match t {
        QbeType::W => 'w',
        QbeType::L => 'l',
        QbeType::S => 's',
        QbeType::D => 'd',
        QbeType::B => 'b',
        QbeType::H => 'h',
    }
}
