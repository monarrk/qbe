use crate::types::{QbeType, type_to_char};
use crate::instruction::{Instruction, InstructionSet};
use crate::dump_instruction_set;

#[derive(Debug)]
/// A module for holding the il
pub struct Module {
    contents: InstructionSet,
}

impl Module {
    pub fn new() -> Module {
        Module {
            contents: Vec::new(),
        }
    }

    /// Add a function to the module
    pub fn add_function(&mut self, name: &str, kind: QbeType, args: Option<Vec<&str>>, export: bool, contents: InstructionSet) {
        let c = type_to_char(kind);
        let instructions = dump_instruction_set!(contents);
        let a = match args {
            Some(v) => v.join(","),
            None => String::new(),
        };
        self.contents.push(Instruction::new(format!("{} function {} ${}({}) {{\n@start\n{}\n}}",
            if export { "export" } else { "" },
            c, 
            name, 
            a,
            instructions)));
    }

    /// Add a data type
    pub fn add_data(&mut self, name: &str, args: Vec<&str>) {
        self.contents.push(Instruction::new(format!("data ${} = {{ {} }}", name, args.join(","))));
    }

    /// Add a raw instruction
    pub fn add_raw(&mut self, contents: InstructionSet) {
        self.contents.push(Instruction::new(dump_instruction_set!(contents)));
    }

    /// Dump the contents into a String
    pub fn dump(&self) -> String {
        let mut s = String::new();
        for i in self.contents.iter() {
            s = s + &i.dump() + "\n";
        }
        s
    }
}
