#[derive(Debug)]
/// A single instruction
pub struct Instruction {
    contents: String,
}

impl Instruction {
    pub fn new(c: String) -> Instruction {
        Instruction {
            contents: c,
        }
    }

    /// Dump the contents to a str
    pub fn dump(&self) -> &str {
        &self.contents
    }
}

/// A simple wrapper for a set of instructions
pub type InstructionSet = Vec<Instruction>;

#[macro_export]
/// Convert an InstructionSet into a String
macro_rules! dump_instruction_set {
    ( $x:expr ) => {
        {
            let mut s = String::new();
            for l in $x.iter() {
                s = s + l.dump() + "\n";
            }
            s
        }
    };
}
